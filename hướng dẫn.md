# Tool
1. Bitvise: https://www.bitvise.com/ssh-client-download
2. Python: https://www.python.org/downloads/
3. Mozila Firefox: https://www.mozilla.org/vi/firefox/new/

# Run setup
Install python package: `pip install -r requirement.txt`

# Config proxy
Copy all proxy to file `proxy.csv` (tạo mới nếu chưa có săn) với format
    `IP:Port`

# Config Link
Format:
    `Plugin name, Link cần click, Username, Password, Login Link, Logout Link`

Trong đó:

- `Plugin name`: là bắt buộc, nếu chỉ đơn giản là truy cập và radom click chọn Base
    - Ví dụ: Base,https://www.google.com/search?q=what+is+etsy,,,
    - List của plugin có thể tìm trong folder /pages, chính là filename
- `Link cần click`: bắt buộc
- `Username`: không bắt buộc, dùng để login vào page
- `Password`: không bắt buộc, dùng để login vào page
- `Login Link`: Link để login
- `Logout Link`: Link để logout


# Build - Optional
1. Build Python to exe file:
`pyinstaller --onefile __main__.py`

2. Build ps1 to exe:
`ps2exe -InputFile run.ps1 -OutputFile release\run.exe`

# Hướng dẫn chạy
1. Right click vào file `run.cmd` và chọn `Run with admin ...`
